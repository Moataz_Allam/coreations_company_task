﻿using AdminPanel_Project.Models;
using AdminPanel_Project.ViewModels;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using X.PagedList;

namespace AdminPanel_Project.Controllers
{
    public class CompaniesController : Controller
    {
        private readonly DataContext _context;

        public CompaniesController(DataContext context)
        {
            this._context = context;
        }

        public async Task<IActionResult> Index(string searchTerm,int? page)
        {
            var companies = await _context.Companies.ToListAsync();


            ViewData["CurrentSearch"] = searchTerm;

            if (!string.IsNullOrWhiteSpace(searchTerm))
            {
                companies = companies.Where(c => c.Name.Contains(searchTerm) || c.Website.Contains(searchTerm))
                                .ToList();
            }

            int pageNumber = page ?? 1;
            var onPageOfCompanies = companies.ToPagedList(pageNumber, 10);

            return View(onPageOfCompanies);
        }


        public IActionResult Add()
        {
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> Add(ComapnyViewModel viewModel)
        {
            if (ModelState.IsValid)
            {
                var employee = new Company
                {
                   Name = viewModel.Name,
                   Website = viewModel.Website,
                   Address = viewModel.Address
                };

                await _context.Companies.AddAsync(employee);
                await _context.SaveChangesAsync();

                return RedirectToAction("Index", "Companies");
            }
            return View(viewModel);
        }
    }
}
