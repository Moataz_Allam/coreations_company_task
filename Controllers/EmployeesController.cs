﻿using AdminPanel_Project.Models;
using AdminPanel_Project.ViewModels;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using X.PagedList;

namespace AdminPanel_Project.Controllers
{
    public class EmployeesController : Controller
    {
        private readonly DataContext _context;

        public EmployeesController(DataContext context)
        {
            this._context = context;
        }

        public async Task<IActionResult> Index(string searchTerm, int? page)
        {
            var employees = await _context.Employees
                                    .Include(e => e.Company)
                                    .ToListAsync();


            ViewData["CurrentSearch"] = searchTerm;

            if (!string.IsNullOrWhiteSpace(searchTerm))
            {
                employees = employees.Where(e => e.Name.Contains(searchTerm) 
                                        || e.Email.Contains(searchTerm)
                                        || e.Company.Name.Contains(searchTerm))
                                        .ToList();
            }

            int pageNumber = page ?? 1;
            var onPageOfEmployees = employees.ToPagedList(pageNumber, 10);

            return View(onPageOfEmployees);
        }

        public IActionResult Add()
        {
            var viewModel = new EmployeeViewModel
            {
                Companies = _context.Companies.ToList()
            };
            return View(viewModel);
        }

        [HttpPost]
        public async Task<IActionResult> Add(EmployeeViewModel viewModel)
        {
            if (ModelState.IsValid)
            {
                var employee = new Employee
                {
                    Name = viewModel.Name,
                    Email = viewModel.Email,
                    CompanyId = viewModel.CompanyId
                };

                await _context.Employees.AddAsync(employee);
                await _context.SaveChangesAsync();

                return RedirectToAction("Index", "Employees");
            }
            viewModel.Companies = _context.Companies.ToList();
            return View(viewModel);
        }
    }
}
