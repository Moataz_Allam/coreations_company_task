﻿using AdminPanel_Project.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AdminPanel_Project.Controllers.API
{
    [Route("api/[controller]")]
    [ApiController]
    public class CompaniesController : ControllerBase
    {
        private readonly DataContext _context;

        public CompaniesController(DataContext context)
        {
            this._context = context;
        }


        [HttpPost]
        public async Task<IActionResult> GetAllCompanies()
        {
            try
            {
                var draw = Request.Form["draw"].FirstOrDefault();
                var start = Request.Form["start"].FirstOrDefault();
                var length = Request.Form["length"].FirstOrDefault();
                var searchValue = Request.Form["search[value]"].FirstOrDefault();
                int pageSize = 10;
                int skip = start != null ? Convert.ToInt32(start) : 0;
                
                var companies = await _context.Companies.ToListAsync();

                if (!string.IsNullOrEmpty(searchValue))
                {
                    companies = companies.Where(c => c.Name.Contains(searchValue) || 
                                                c.Address.Contains(searchValue) || 
                                                c.Website.Contains(searchValue))
                                                .ToList();
                }

                var numberOfCompanies = companies.Count();
                var data = companies.Skip(skip).Take(pageSize).ToList();

                var response = new
                {
                    draw = draw,
                    recordsFiltered = numberOfCompanies,
                    recordsTotal = numberOfCompanies,
                    data = data
                };
                return Ok(response);

            }
            catch(Exception)
            {
                throw new Exception("Something get wrong!!");
            }





        }
    }
}
