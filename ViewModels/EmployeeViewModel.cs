﻿using AdminPanel_Project.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace AdminPanel_Project.ViewModels
{
    public class EmployeeViewModel
    {
        [Required]
        [MaxLength(100,ErrorMessage ="Maximum should be 100 characters.")]
        public string Name { get; set; }

        [MaxLength(50,ErrorMessage ="Maximun should be 50 character")]
        [EmailAddress(ErrorMessage ="Invalid Email format.")]
        public string Email { get; set; }

        [Required(ErrorMessage ="Please select company name.")]
        public int CompanyId { get; set; }
        public IEnumerable<Company> Companies { get; set; }

    }
}
