﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace AdminPanel_Project.ViewModels
{
    public class ComapnyViewModel
    {
        [Required]
        [MaxLength(100, ErrorMessage = "Maximum should be 100 characters.")]
        public string Name { get; set; }

        [Url(ErrorMessage = "Invalid url format.")]
        [Required]
        public string Website { get; set; }

        [MaxLength(ErrorMessage = "Maximun should be 300 characters.")]
        public string Address { get; set; }
    }
}
