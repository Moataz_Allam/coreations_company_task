﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace AdminPanel_Project.Models
{
    public class Company
    {
        public int Id { get; set; }
        public string Name { get; set; }
        
        [Url]
        public string Website { get; set; }
        public string Address { get; set; }
        public ICollection<Employee> Employees { get; set; }
    }
}
